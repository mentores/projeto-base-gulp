const gulp = require('gulp')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify')
const sass = require('gulp-sass')
const cssmin = require('gulp-cssmin')
const rename = require('gulp-rename')
const sourceMap = require('gulp-sourcemaps')
const browserSync = require('browser-sync').create()
const plumber = require('gulp-plumber');
const notify =  require('gulp-notify');
const php = require('gulp-connect-php');

// -- Comandos:
// gulp
// gulp styles
// gulp js

// -- Coloca Source Map e Minifica e Concatena o arquivo "app.min.js"
gulp.task('ug-concat-script', () => {
    return gulp.src('assets/js/app.js')
    .pipe(plumber({ errorHandler: notify.onError('<%= error.message %>') }))
    .pipe(sourceMap.init())
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(notify({
        title: 'Gulp',
        subtitle: 'Success',
        message: 'Js Compiled',
        sound: 'Pop',
    }))
    .pipe(rename('app.min.js'))
    .pipe(sourceMap.write('/'))
    .pipe(gulp.dest('assets/js'))
    .pipe(browserSync.stream())
})

// -- Compila o arquivo "app.scss" e coloca SourceMap no arquivo "app.css"
gulp.task('comp-sass', function(){
    return gulp.src('assets/sass/app.scss')
    .pipe(plumber({ errorHandler: notify.onError('<%= error.message %>') }))
    .pipe(sourceMap.init())
    .pipe(sass())
    .pipe(notify({
        title: 'Gulp',
        subtitle: 'Success',
        message: 'Sass Compiled',
        sound: 'Pop',
    }))
    .pipe(sourceMap.write('/'))
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.stream())
})

// -- Minifica o arquivo "app.css"
gulp.task('ug-css', ['comp-sass'], () => {
    return gulp.src('assets/css/app.css')
    .pipe(cssmin())
    .pipe(rename('app.min.css'))
    .pipe(gulp.dest('assets/css'))
})

// -- Sempre quando um arquivo Sass for alterado, dispará as tasks de compilaçao
gulp.task('styles', () => {
    gulp.watch([
        'assets/sass/**/*.sass',
        'assets/sass/**/*.scss',
        'assets/sass/*.sass',
        'assets/sass/*.scss'
    ], ['ug-css'])
})

// -- Sempre quando um arquivo Js for alterado, dispará as tasks de compilaçao
gulp.task('js', () => {
    gulp.watch([
        'assets/js/app.js',
    ], ['ug-concat-script'])
})

// -- Sempre quando um arquivo Sass ou Js for alterado, dispará as tasks de compilaçao
gulp.task('watchfile', () => {
    gulp.watch([
        'assets/sass/**/*.sass',
        'assets/sass/**/*.scss',
        'assets/sass/*.sass',
        'assets/sass/*.scss'
    ], ['ug-css'])
    gulp.watch('assets/js/app.js', ['ug-concat-script'])
})

// -- Inicia o Servidor ao vivo em HTML
gulp.task('serve', ['watchfile'], () => {
    browserSync.init({
        server: {
            basDir: './'
        }
    })
    gulp.watch('*.html', browserSync.reload)
})

// -- Inicia o Servidor ao vivo em PHP
gulp.task('serve-php', function(){
    php.server({base:'./', port:8010, keepalive:true});
})

gulp.task('browserSync',['serve-php'], function() {
    browserSync.init({
        proxy:"localhost:8010",
        baseDir: "./",
        open:true,
        notify:false
    })
})

gulp.task('php', ['browserSync', 'watchfile'], function() {
    gulp.watch('./*.php', browserSync.reload);
})

// Task default "gulp" que dispara a task "serve" ou "php", dependendo do que deseja
gulp.task('default', ['php'])